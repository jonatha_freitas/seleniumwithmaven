package testes;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class OpenSiteTeste {
	
	private WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\WS_ADS4_Aplica��es\\WebDriver\\bin\\chromedriver.exe");
		driver= new ChromeDriver();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException {
		driver.get("http://localhost:4200/contact-create");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id='name']")).sendKeys("Anthonny Max");
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("AnthonnyMax@hotmail.com");
		driver.findElement(By.xpath("//*[@id='description']")).sendKeys("Este � um teste utilizando Selenium ");
		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[contains(.,'Criar contato')]")).click();
		Thread.sleep(2000);
		driver.get("http://localhost:4200/contact-list");
		assertTrue("Titulo da P�gina difere do esperado", driver.getTitle().contentEquals("Frontend"));
		Thread.sleep(5000);
	}

}
